package com.openinfotech.weather;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.openinfotech.weather.adapter.ForecastAdapter;
import com.openinfotech.weather.helper.AppController;
import com.openinfotech.weather.models.Forecast;
import com.openinfotech.weather.utils.SetListViewHeight;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private TextView mTemp, mHumidity, mTempHigh, mTempLow, mName, mWeather, mWeatherIcon;
    private ListView mListViewForecast;
    private List<Forecast> arrayListForecast;
    private Handler handler;
    private LocationManager locationManager;
    private long tenMin = 10 * 60 * 1000;
    private long oneMile = 1609; // meters

    private double Latitude = 18.516726;
    private double Longitude = 73.856255;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Typeface weatherFont = Typeface.createFromAsset(getAssets(), "fonts/Weather-Fonts.ttf");
        Typeface robotoThin = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Thin.ttf");
        Typeface robotoLight = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");

        initForecastView();


        mWeatherIcon.setTypeface(weatherFont);
        mTemp.setTypeface(robotoThin);
        mName.setTypeface(robotoLight);
        mWeather.setTypeface(robotoLight);

        handler = new Handler();
        arrayListForecast = new ArrayList<>();

        renderCurrentWeather();
        renderForecastWeather();

    }

    private void initForecastView() {

        mListViewForecast = (ListView) findViewById(R.id.listView);
        mListViewForecast.setEnabled(false);
        mTemp = (TextView) findViewById(R.id.temp);
        mHumidity = (TextView) findViewById(R.id.humidity);
        mTempHigh = (TextView) findViewById(R.id.tempHigh);
        mTempLow = (TextView) findViewById(R.id.tempLow);
        mName = (TextView) findViewById(R.id.name);
        mWeather = (TextView) findViewById(R.id.weather);
        mWeatherIcon = (TextView) findViewById(R.id.weatherIcon);

    }


    private void renderCurrentWeather() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                "http://api.openweathermap.org/data/2.5/weather?lat=18.516726&lon=73.856255&units=imperial", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    JSONObject weather = json.getJSONArray("weather").getJSONObject(0);
                    JSONObject main = json.getJSONObject("main");
                    mName.setText(json.getString("name"));
                    mWeather.setText(weather.getString("description"));
                    mTemp.setText(main.getLong("temp") + "" + (char) 0x00B0);
                    mTempHigh.setText("MAX: " + main.getLong("temp_max") + "" + (char) 0x00B0);
                    mTempLow.setText("MIN: " + main.getLong("temp_min") + "" + (char) 0x00B0);
                    mHumidity.setText("Humidity " + main.getString("humidity") + "%");
                    setWeatherIcon(weather.getInt("id"), json.getJSONObject("sys").getLong("sunrise") * 1000, json.getJSONObject("sys").getLong("sunset") * 1000);
                } catch (JSONException e) {
                    Log.e("CURRENT_JSON_ERROR", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("x-api-key", "05d14a8762a1fb6f0b996b87b33e84ee");
                return map;
            }
        };
        AppController.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void renderForecastWeather() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                "http://api.openweathermap.org/data/2.5/forecast?lat=18.516726&lon=73.856255&units=imperial",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            arrayListForecast.clear(); // clear list, prevent duplicates on refresh
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray list = jsonObject.getJSONArray("list");
                            for (int i = 0; i < 6; i++) {
                                JSONObject listItem = list.getJSONObject(i);
                                JSONObject temp = listItem.getJSONObject("main");
                                JSONObject weather = listItem.getJSONArray("weather").getJSONObject(0);
                                Forecast forecast = new Forecast();
                                forecast.setHighTemp(String.valueOf(temp.getLong("temp_max")));
                                forecast.setLowTemp(String.valueOf(temp.getLong("temp_min")));
                                forecast.setWeather(weather.get("description").toString());
                                forecast.setWeatherId(weather.get("id").toString());
                                arrayListForecast.add(forecast);
                            }
                            ForecastAdapter testAdapter = new ForecastAdapter(MainActivity.this, 0, arrayListForecast);
                            mListViewForecast.setAdapter(testAdapter);
                            SetListViewHeight.setListViewHeight(mListViewForecast);
                            testAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            Log.e("FORECAST_JSON_ERROR", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("x-api-key", "05d14a8762a1fb6f0b996b87b33e84ee");
                return map;
            }
        };

        AppController.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void setWeatherIcon(int actualId, long sunrise, long sunset) {
        int id = actualId / 100;
        String icon = "";
        if (actualId == 800) {
            long currentTime = new Date().getTime();
            if (currentTime >= sunrise && currentTime < sunset) {
                icon = getString(R.string.weather_sunny);
            } else {
                icon = getString(R.string.weather_clear_night);
            }
        } else {
            switch (id) {
                case 2:
                    icon = getString(R.string.weather_thunder);
                    break;
                case 3:
                    icon = getString(R.string.weather_drizzle);
                    break;
                case 7:
                    icon = getString(R.string.weather_foggy);
                    break;
                case 8:
                    icon = getString(R.string.weather_cloudy);
                    break;
                case 6:
                    icon = getString(R.string.weather_snowy);
                    break;
                case 5:
                    icon = getString(R.string.weather_rainy);
                    break;
            }
        }
        mWeatherIcon.setText(icon);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(this); // remove the location updates
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, tenMin, oneMile, this);
    }

    @Override
    public void onLocationChanged(Location location) {

        // updateWeather(Latitude + "", Longitude + "");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
